/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2nuevo;

import java.util.ArrayList;

/**
 *
 * @author Usuario
 */
public class objetopersona {
        private int cedula;
    private String nombre;
    private String generp;
    public static ArrayList<objetopersona> listapersonas= new ArrayList<>();

    public objetopersona(int cedula, String nombre, String generp) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.generp = generp;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenerp() {
        return generp;
    }

    public void setGenerp(String generp) {
        this.generp = generp;
    }

    public static ArrayList<objetopersona> getListapersonas() {
        return listapersonas;
    }

    public static void setListapersonas(ArrayList<objetopersona> listapersonas) {
        objetopersona.listapersonas = listapersonas;
    }
}
