/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2nuevo;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class Lab2nuevo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner opc = new Scanner(System.in);
        int opcion;
        boolean salir = false;
        while (!salir) {
            System.out.println("");
            System.out.println("MENÚ PRINCIPAL");
            System.out.println("");
            System.out.println("1. Registro de personas");
            System.out.println("2. Registro de mascotas");
            System.out.println("3. Adoptar mascotas");
            System.out.println("4. Salir");
            System.out.println("Ingrese la opcion que desea realizar:");
            opcion = opc.nextInt();
            switch (opcion) {

                case 1:
                    registropersona regp = new registropersona();
                    regp.listregisper();
                    break;
                case 2:
                    registromascota regm = new registromascota();
                    regm.listregismascotas();

                    break;
                case 3:
                    adoptarmascota adop= new adoptarmascota();
                    adop.adopmasc();

                    break;
                case 4:
                    salir = true;
                    System.out.println("Opcion de salida");
                    break;
            }

        }
    }
}


