/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2nuevo;

import java.util.ArrayList;

/**
 *
 * @author Usuario
 */
public class objetosmascota {
        private int cedula;
    private String nombre;
    private String tipo;
    private String estado;
    public static ArrayList<objetosmascota>listamascotas =new ArrayList<>();

    public objetosmascota(int cedula, String nombre, String tipo, String estado) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.tipo = tipo;
        this.estado = estado;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public static ArrayList<objetosmascota> getListamascotas() {
        return listamascotas;
    }

    public static void setListamascotas(ArrayList<objetosmascota> listamascotas) {
        objetosmascota.listamascotas = listamascotas;
    }
}
